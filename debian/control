Source: aiohttp-apispec
Section: python
Priority: optional
Maintainer: Kali Developers <devel@kali.org>
Uploaders: Sophie Brun <sophie@offensive-security.com>
Build-Depends: debhelper-compat (= 13),
               dh-python,
               python3-all,
               python3-marshmallow,
               python3-pytest,
               python3-pytest-cov,
#               python3-pytest-aiohttp,
               python3-aiohttp,
               python3-apispec,
#               python3-webargs,
               python3-jinja2,
               python3-setuptools,
               python3-sphinx,
               python3-sphinx-rtd-theme
Standards-Version: 4.6.2
Homepage: https://github.com/maximdanilchenko/aiohttp-apispec
Vcs-Browser: https://gitlab.com/kalilinux/packages/aiohttp-apispec
Vcs-Git: https://gitlab.com/kalilinux/packages/aiohttp-apispec.git
Testsuite: autopkgtest-pkg-python
Rules-Requires-Root: no

Package: python3-aiohttp-apispec
Architecture: all
Depends: ${misc:Depends}, ${python3:Depends}
Suggests: python-aiohttp-apispec-doc
Description: Build and document REST APIs with aiohttp and apispec (Python 3)
 This package contains a Python module to build and document REST APIs with
 aiohttp and apispec.
 The key features are:
    - docs and request_schema decorators to add swagger spec support out of the
      box;
    - validation_middleware middleware to enable validating with marshmallow
      schemas from those decorators;
    - SwaggerUI support.
    - match_info_schema, querystring_schema, form_schema, json_schema,
      headers_schema and cookies_schema decorators for specific request parts
      validation
 .
 This package installs the library for Python 3.

Package: python-aiohttp-apispec-doc
Architecture: all
Section: doc
Depends: ${misc:Depends}, ${sphinxdoc:Depends}
Description: Build and document REST APIs with aiohttp and apispec (common documentation)
 This package contains a Python module to build and document REST APIs with
 aiohttp and apispec.
 The key features are:
    - docs and request_schema decorators to add swagger spec support out of the
      box;
    - validation_middleware middleware to enable validating with marshmallow
      schemas from those decorators;
    - SwaggerUI support.
    - match_info_schema, querystring_schema, form_schema, json_schema,
      headers_schema and cookies_schema decorators for specific request parts
      validation
 .
 This is the common documentation package.
